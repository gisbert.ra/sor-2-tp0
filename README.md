<h1>Consigna:</h1>
    1. Definir funciones init module y cleanup module <br>
    • ¿Que necesito que hagan en un char device?<br>
    2. Definir funciones device open y device release<br>
    3. Hacer que nuestro char device cuando le escribimos imprima en el kernel<br>
    • Tip 1: Para escribir a un device en Bash nano /dev/device<br>
    • Tip 2: Ojo con los permisos de escritura!<br>
    • Tip 3: Al de-registrar y volver a registrar el driver repetir el proceso de crear el
    archivo, sino esto trae problemas. Mismo no olvidar hacer make clean<br>
    4. Hacer que nuestro char device cuando lo lea me devuelva lo ultimo que fue escrito<br>
    • Tip 4: cat /dev/device me tiene que devolver lo que puse con echo<br>
    5. Hacer que ahora devuelva el mensaje al reves caracter por caracter, por ej si el mensaje<br>
    es hola: aloh<br>

<br>
<h1>Contenido del proyecto:</h1>
El proyecto tiene un archivo <i>.c</i> con el código del driver que satisface la consigna del tp, el archivo <i>Makefile</i> para compilar y un archivo de aliases de bash que utilicé para las pruebas.<br>

<strong>MKNOD</strong><br>
La máquina virtual siempre me generaba el módulo con el major number 240, por eso el comando mknod tiene ese valor.<br>
Además de eso, gracias a las man pages de MKNOD encontré el parámetro -m que está relacionado a los permisos.
