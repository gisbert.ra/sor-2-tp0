#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#define DRIVER_AUTHOR "Ricardo Gisbert"
#define DRIVER_DESC "An amazing driver for a spectacular newdevice"

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "newdevice"
#define BUF_LEN 80

static int Major;
static int Device_Open = 0;

static char msg[BUF_LEN];
static char *msg_Ptr;

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

int init_module(void){
    Major = register_chrdev(0, DEVICE_NAME, &fops);
    
    if(Major < 0){
        printk(KERN_ALERT "Registering char device failed with %d\n", Major);
        return Major;
    }

    printk(KERN_INFO "Char Device registered with major number: %d.\n", Major);        
    return SUCCESS;
}

void cleanup_module(void){        
    /* Unregister the device */        
    printk(KERN_INFO "Removing the char device %s.\n",DEVICE_NAME);    
    unregister_chrdev(Major, DEVICE_NAME);          
}

static int device_open(struct inode *inode, struct file *file){              
    if (Device_Open)                
        return -EBUSY;        
    
    printk(KERN_INFO "Opening device %s.\n",DEVICE_NAME);    

    Device_Open++;        
    msg_Ptr = msg;        
    try_module_get(THIS_MODULE);        
    return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file){        
    Device_Open--;          
    printk(KERN_INFO "Releasing the char device %s.\n",DEVICE_NAME);    
    module_put(THIS_MODULE);        
    return 0;
}

static ssize_t device_read(struct file *filp,   /* see include/linux/fs.h   */                           
        char *buffer,        /* buffer to fill with data */                           
        size_t length,       /* length of the buffer     */                           
        loff_t * offset){        
    
    
    /* Number of bytes actually written to the buffer*/        
    int bytes_read = 0;        

    /* If we're at the end of the message, return 0 signifying end of file */        
    if (*msg_Ptr == 0)                
        return 0;        
    
    /* Actually put the data into the buffer */        
    while (length && *msg_Ptr) {                
        /*                  
         * The buffer is in the user data segment, not the kernel                  
         * segment so "*" assignment won't work.  We have to use                  
         * put_user which copies data from the kernel data segment to                 
         * the user data segment.                  
         */                
        put_user(*(msg_Ptr++), buffer++);                
        length--;                
        bytes_read++;        
    }  

    printk(KERN_INFO "Reading the char device %s.\n",DEVICE_NAME);    

/* Most read functions return the number of bytes put into the buffer */        
    return bytes_read;
}

/* Called when a process writes to dev file: echo "hi" > /dev/hello */
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off){        
    
    int i, j;
    j = len - 1;

    for(i=0; i < len && i < BUF_LEN; i++){
        get_user(msg[j] , buff + i);
        j--;
    }
    
    msg[len] = '\n';
    msg[len + 1] = '\0';
    
    printk(KERN_INFO "Writing to the char device %s.\n",DEVICE_NAME);    
    return len;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
